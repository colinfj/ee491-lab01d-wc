# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c argParser.c errors.c printWC.c fileProcess.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c argParser.c errors.c printWC.c fileProcess.c

test:
	./wc --version
	wc -lc test1
	./wc -lc test1
	wc -cw test2
	./wc -cw test2
	wc --bytes --words --lines test3
	./wc --bytes --words --lines test3
	wc test1 test2 test3
	./wc test1 test2 test3
	./wc --help

clean:
	rm $(TARGET)

