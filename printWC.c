///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file printWC.c
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////
#include "printWC.h"


/*
 * Function: printHelp
 *
 * Description: Prints a help message to user
 */
void printHelp()
{
   printf("  Usage: wc [OPTION]... [FILE]...\n");
   printf("  Print newline, word, and byte counts for each FILE, and a total line if\n");
   printf("  more than one FILE is specified. A word is a non-zero-length sequence of\n");
   printf("  characters delimited by whitespace.\n\n");
   printf("  The options below may be used to select which counts are printed, always in\n");
   printf("  the following order: newline, word, character.\n");
   printf("  -c, --bytes\tprint the byte counts\n");
   printf("  -l, --lines\tprint the newline counts\n");
   printf("  -w, --words\tprint the word counts\n");
   printf("   --help    \tdisplay this help and exit\n");
   printf("   --version \t output version information and exit\n");
}


/*
 * Funtion: printVersion
 *
 * Description: Prints the program version
 */
void printVersion()
{
   printf("%s %s\n", PROGRAM, VERSION);
}


/*
 * Function: printCounts
 *
 * Description: prints out counts depending on active flags
 *
 * @param: filename[] - the name of the file that was processed.
 *         flags - flag structure for which options have been selected.
 *         lines - count of lines
 *         words - count of words
 *         characters - count of characters.
 */
void printCounts(char filename[], struct Flags flags, int lines, int words, int characters)
{
   if(!flags.enableLines && !flags.enableBytes && !flags.enableWords) //in the case where no arguments are passed the default is to print all counts
   {
      printf("%d\t%d\t%d\t", lines, words, characters);
   }
   else //only some of the options are enabled
   {
      if(flags.enableLines)
      {
         printf("%d\t", lines);
      }

      if(flags.enableWords)
      {
         printf("%d\t", words);
      }

      if(flags.enableBytes)
      {
         printf("%d\t", characters);
      }
   }

   printf("%s\n", filename);
}
