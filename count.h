///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file count.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////

#ifndef COUNT_H
#define COUNT_H

/*
 * Structure containing the counts of words, chars, bytes, and their totals
 * if multiple files are being counted.
 */
struct Count
{
   int lineCount, wordCount, characterCount, lineTotal, wordTotal, characterTotal;
};

#endif
