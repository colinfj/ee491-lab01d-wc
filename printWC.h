///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file printWC.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef PRINTWC_H
#define PRINTWC_H

#include <stdio.h>
#include <stdlib.h>
#include "flags.h"
#include "wcDefinitions.h"

void printHelp();
void printVersion();
void printCounts(char filename[], struct Flags flags, int lines, int words, int characters);

#endif
