///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file errors.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef ERRORS_H
#define ERRORS_H

#include <stdio.h>
#include <stdlib.h>
#include "wcDefinitions.h"

void printErrorMsg(int, char []);

#endif
