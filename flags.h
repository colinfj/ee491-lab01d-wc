///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file flags.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef FLAGS_H
#define FLAGS_H

#include <stdbool.h>


/*
 * Flags structure maintains what options have been enabled if any at all.
 */
struct Flags {
   bool enableLines, enableWords, enableBytes;
};

#endif
