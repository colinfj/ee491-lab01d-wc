
///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file argParser.C
/// @version 2.1
/// @see https://linux.die.net/man/3/getopt for documentation on getopt_long() uses
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////

#include "argParser.h"
#include "wcDefinitions.h"

static int opt = 0;

/*
 *structure containing all valid options for wc program
 */

static struct option long_options[] = {
   {"bytes", optional_argument, 0, 'c'},
   {"words", optional_argument, 0, 'w'},
   {"lines", optional_argument, 0, 'l'},
   {"version", optional_argument, 0, 'v'},
   {"help", optional_argument, 0, 'h'},
   {0, 0, 0, 0}
};



/*
 * Function: enableOptions
 *
 * @Param: argc - number of arguments from command line
 *         *argv[] - pointer array to arguments from command line
 *         *flags - pointer to flag struct
 *
 * @return 0 - continued running of program with enabled flags
 *         Interupt code - returns interupt code to be handled
 *                         by name.
 */
int enableOptions(int argc, char *argv[], struct Flags *flags)
{
   while( (opt = getopt_long(argc, argv, "clwvh", long_options, NULL)) != -1)
   {
      switch(opt)
      {
         case 'c':
            flags->enableBytes = true;
            break;
         case 'w':
            flags->enableWords = true;
            break;
         case 'l':
            flags->enableLines = true;
            break;
         case 'v':
            return PRINT_VERSION;
         case 'h':
            return PRINT_HELP;
         default: ;
            return ILLEGAL_ARG;
      }
   }

   return 0;
}

