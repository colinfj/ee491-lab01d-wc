///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file fileProcess.c
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////

#include "fileProcess.h"


/*
 * Function: fileProcess
 *
 * Description: Process the files for wc program, keeping track of required counts.
 *
 * @param *argv[] - pointer array of arguments from command line
 *        argc - number of arguments from command line
 *        fileIndex - copy of index where file names start in argv
 *        *fileptr - ptr to the file
 *        flags - copy of flags structs to pass to printCounts
 *        *counter - pointer to Count structure
 *
 * @Return numOfFiles - number of files processed.
 */
int fileProcess(char *argv[], int argc, int fileIndex, FILE *filePtr, struct Flags flags, struct Count *counter)
{
   char ch;
   bool inWord = false;
   int numOfFiles = 0;

   while(fileIndex < argc)
   {
      ch = fgetc(filePtr);
      numOfFiles++;

      while(ch != EOF)
      {
         counter->characterCount++;

         if(inWord)
         {
            if(ch == ' ' || ch == '\t')
            {
               counter->wordCount++;
               inWord = false;
            } 
            else if(ch == '\n')
            {
               counter->wordCount++;
               counter->lineCount++;
               inWord = false;
            }
         }
         else
         {
            if(ch == '\n')
            {
               counter->lineCount++;
            }

            if(ch >= 0x21 && ch <= 0x7E)
            {
               inWord = true;
            }
         }
            
         ch = fgetc(filePtr);
       }

      printCounts(argv[fileIndex], flags, counter->lineCount, counter->wordCount, counter->characterCount);
      addToTotals(counter);

      fileIndex++;

      if(fileIndex < argc)
      {
         fclose(filePtr);
         filePtr = fopen(argv[fileIndex], "r");
      }
   }

   fclose(filePtr);

   return numOfFiles;
}


/*
 * Function: addToTotals
 *
 * Description: adds the count for current file to the total count.
 *
 * @param *counter - pointer to the Count structure
 */
void addToTotals(struct Count *counter)
{
   counter->wordTotal += counter->wordCount;
   counter->lineTotal += counter->lineCount;
   counter->characterTotal += counter->characterCount;

   counter->wordCount = 0;
   counter->lineCount = 0;
   counter->characterCount = 0;
}
