///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file fileProcess.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////

#ifndef FILEPROCESS_H
#define FILEPROCESS_H

#include <stdio.h>
#include <stdbool.h>
#include "wcDefinitions.h"
#include "printWC.h"
#include "flags.h"
#include "count.h"

int fileProcess(char *argv[], int argc, int fileIndex, FILE *filePtr, struct Flags flags, struct Count *counter);
void addToTotals(struct Count *counter);

#endif
