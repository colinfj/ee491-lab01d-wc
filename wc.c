///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "count.h"
#include "argParser.h"
#include "flags.h"
#include "errors.h"
#include "printWC.h"
#include "fileProcess.h"
#include "wcDefinitions.h"


void initializeVars(struct Count *counter, struct Flags *flags);
void addToTotals(struct Count *counter);


/*
 * Function: main
 *
 * Description: main driver for wc program
 *
 * return 0 - successful completion of program.
 */
int main(int argc, char *argv[])
{
   FILE *filePtr;
   int fileIndex = 1,
       numOfFiles = 0,
       argParserCode;
   struct Count counter;
   struct Flags flags;

   if(argc < 2)
   {
      printErrorMsg(NO_ARG_ERROR, "");
      exit(EXIT_FAILURE);
   }

   initializeVars(&counter, &flags);
   argParserCode = enableOptions(argc, argv, &flags);

   switch(argParserCode)
   {
      case PRINT_HELP:
         printHelp();
         exit(EXIT_SUCCESS);
      case PRINT_VERSION:
         printVersion();
         exit(EXIT_SUCCESS);
      case ILLEGAL_ARG:
         printErrorMsg(ILLEGAL_ARG, "");
         exit(EXIT_FAILURE);
      default:
         break;
   }

   while(fileIndex < argc && argv[fileIndex][0] == '-')
   {
      fileIndex++;
   }

   filePtr = fopen(argv[fileIndex], "rb");

   if(filePtr == NULL)
   {
      printErrorMsg(FILE_ERROR, argv[fileIndex]);
      exit(EXIT_FAILURE);
   }

   numOfFiles = fileProcess(argv, argc, fileIndex, filePtr, flags, &counter);

   if(numOfFiles > 1)
   {
      printCounts("Totals", flags, counter.lineTotal, counter.wordTotal, counter.characterTotal);
   }
         
   return EXIT_SUCCESS;
}


/*
 * Function: intializeVars
 *
 * Description: Initializes variables to be used in main and supporting functions
 *
 * @param: *counter - pointer to Count struct for totals of lines, bytes, words
 *         *flags - pointer to flags struct for enabling of option.
 */
void initializeVars(struct Count *counter, struct Flags *flags)
{
   counter->lineCount = 0;
   counter->wordCount = 0;
   counter->characterCount = 0;
   counter->lineTotal = 0;
   counter->wordTotal = 0;
   counter->characterTotal = 0;
   flags->enableBytes = false;
   flags->enableLines = false;
   flags->enableWords = false;
}
   
