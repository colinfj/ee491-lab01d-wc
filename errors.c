///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file errors.c
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////

#include "errors.h"


/*
 * Function: printErrorMsg()
 *
 * Description: Prints errors message to console.
 *
 * @param: errorCode - The error code received.
 *         file [] - c-string of the file name.
 *
 */
void printErrorMsg(int errorCode, char file[])
{
   switch(errorCode)
   {
      case NO_ARG_ERROR:
         fprintf(stderr, "%s: Usage: [OPTIONS] [FILE]\n", PROGRAM);
         printf("Try '%s --help' for more information\n", PROGRAM); 
         break;
      case FILE_ERROR:
         fprintf(stderr, "%s: Can't open [%s]\n", PROGRAM, file);
         break;
      case ILLEGAL_ARG:
         printf("Try '%s --help' for more information\n", PROGRAM);
         break;
      default:
         fprintf(stderr, "%s: Unrecognized error [%d]\n", PROGRAM, errorCode);
         break;
   }
}

