///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wcDefinitions.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef WCDEFINITIONS_H
#define WCDEFINITIONS_H

#define PROGRAM "wc"
#define VERSION "2.1"
#define NO_ARG_ERROR 1
#define FILE_ERROR 2
#define ILLEGAL_ARG 3
#define PRINT_HELP 101
#define PRINT_VERSION 102

#endif
